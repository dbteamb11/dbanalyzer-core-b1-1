/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.core;

import com.deutsche.dba.dbutils.Counterparty;
import com.deutsche.dba.dbutils.Deal;
import com.deutsche.dba.dbutils.DealHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.deutsche.dba.MainUnit;
import com.deutsche.dba.dbutils.User;
import com.deutsche.dba.tools.JsonHelper;

import java.util.List;

/**
 *
 * @author Selvyn
 */
public class UserController
{
    final   private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public  String  verifyLoginDetails( String userId, String userPwd )
    {
        String result = null;
 
        User theUser = ash.userLogin(userId, userPwd);

        try {
            if (theUser != null)
                result = JsonHelper.toJSON(theUser);
        } catch (JsonProcessingException e) {
            MainUnit.log(String.format("Can not convert user to JSON du to error: %s", e.toString()));
        }
        return result;
    }

    public List<String> getAllUsers() {
        return ash.getUserNames();
    }

    public List<String> getAllInstrumentNames() {
        return ash.getInstrumentNames();
    }

    public Counterparty getCounterpartyByName(String counterPartyName) {
        return ash.getCounterPartyByName(counterPartyName);
    }

    public List<Deal> getDealsByCounterpartyName(String counterPartyName) {
        return ash.getDealsByCounterpartyName(counterPartyName);
    }

    public List<Deal> getDealsByInstrumentName(String instrumentName) {
        return ash.getDealsByInstrumentName(instrumentName);
    }

    public List<Deal> getAllDeals() {
        return ash.getAllDeals();
    }

    public List<DealHandler.DealPosition> getDealPositionsByCounterpartyName(String counterPartyName) {
        return ash.getDealPositionsByCounterPartyName(counterPartyName);
    }

    public List<DealHandler.DealPosition> getDealPositionsByInstrumentName(String instrumentName) {
        return ash.getDealPositionsByInstrumentName(instrumentName);
    }
}
