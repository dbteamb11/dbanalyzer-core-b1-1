/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.core;

import com.deutsche.dba.MainUnit;
import com.deutsche.dba.dbutils.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Selvyn
 */
public class ApplicationScopeHelper
{

    private String itsInfo = "NOT SET";
    private DBConnector itsConnector = null;

    public String getInfo()
    {
        return itsInfo;
    }

    public void setInfo(String itsInfo)
    {
        this.itsInfo = itsInfo;
    }

    public boolean bootstrapDBConnection()
    {
        boolean result = false;
        if (itsConnector == null)
        {
            try
            {
                itsConnector = DBConnector.getInstance();

                PropertyLoader pLoader = PropertyLoader.getLoader();

                Properties pp;
                pp = pLoader.getPropValues("dbConnector.properties");

                result = itsConnector.connect(pp);
            } catch (IOException ex)
            {
                Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
            result = true;

        return result;
    }

    public User userLogin( String userId, String userPwd )
    {
        User theUser = null;
        try
        {
            UserHandler theUserHandler = UserHandler.getLoader();

            theUser = theUserHandler.loadFromDB(
                    DBConnector.getInstance().getConnection(),
                    DBConnector.getInstance().getDbName(),
                    userId,
                    userPwd
            );

            if( theUser != null )
                MainUnit.log( "User " + userId + " has logged into system");
        } catch (IOException ex)
        {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return theUser;
    }

    public List<String> getUserNames() {
        CounterpartyHandler counterpartyHandler = CounterpartyHandler.getLoader();
        List<String> result = new ArrayList<>();
        try {
            return counterpartyHandler.getAllCounterparties(
                    DBConnector.getInstance().getConnection(),
                    DBConnector.getInstance().getDbName()
            )
                    .stream()
                    .map(Counterparty::getName)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            MainUnit.log(e.toString());
        }
        return result;
    }

    public List<String> getInstrumentNames() {
        InstrumentHandler instrumentHandler = InstrumentHandler.getLoader();
        List<String> result = new ArrayList<>();
        try {
            return instrumentHandler.loadAllFromDB(
                    DBConnector.getInstance().getConnection(),
                    DBConnector.getInstance().getDbName()
            )
                    .stream()
                    .map(Instrument::getInstrumentName)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            MainUnit.log(e.toString());
        }
        return result;
    }

    public Counterparty getCounterPartyByName(String counterPartyName) {
        Counterparty counterparty = null;
        CounterpartyHandler handler = CounterpartyHandler.getLoader();
        try {
            counterparty = handler.loadFromDB(
                    DBConnector.getInstance().getConnection(),
                    DBConnector.getInstance().getDbName(),
                    counterPartyName
            );
        } catch (IOException e) {
            MainUnit.log(e.toString());
        }
        return counterparty;
    }

    public List<Deal> getDealsByCounterpartyName(String counterPartyName) {
        DealHandler handler = DealHandler.getLoader();
        List<Deal> result = new ArrayList<>();
        try {
            return handler.getFromDBAsListByCounterPartyName(
                    DBConnector.getInstance().getDbName(),
                    DBConnector.getInstance().getConnection(),
                    counterPartyName
            );
        } catch (IOException e) {
            MainUnit.log(e.toString());
        }
        return result;
    }

    public List<Deal> getDealsByInstrumentName(String instrumentName) {
        DealHandler handler = DealHandler.getLoader();
        List<Deal> result = new ArrayList<>();
        try {
            return handler.getFromDBAsListByInstrumentName(
                    DBConnector.getInstance().getDbName(),
                    DBConnector.getInstance().getConnection(),
                    instrumentName
            );
        } catch (IOException e) {
            MainUnit.log(e.toString());
        }
        return result;
    }

    public List<Deal> getAllDeals() {
        DealHandler handler = DealHandler.getLoader();
        List<Deal> result = new ArrayList<>();
        try {
            return handler.getFromDBAsList(
                    DBConnector.getInstance().getDbName(),
                    DBConnector.getInstance().getConnection()
            );
        } catch (IOException e) {
            MainUnit.log(e.toString());
        }
        return result;
    }

    public List<DealHandler.DealPosition> getDealPositionsByCounterPartyName(String counterPartyName) {
        DealHandler handler = DealHandler.getLoader();
        List<DealHandler.DealPosition> result = new ArrayList<>();
        try {
            return handler.getTimeSeriesByCounterPartyName(
                    DBConnector.getInstance().getDbName(),
                    DBConnector.getInstance().getConnection(),
                    counterPartyName
            );
        } catch (IOException e) {
            MainUnit.log(e.toString());
        }
        return result;
    }

    public List<DealHandler.DealPosition> getDealPositionsByInstrumentName(String instrumentName) {
        DealHandler handler = DealHandler.getLoader();
        List<DealHandler.DealPosition> result = new ArrayList<>();
        try {
            return handler.getTimeSeriesByInstrumentName(
                    DBConnector.getInstance().getDbName(),
                    DBConnector.getInstance().getConnection(),
                    instrumentName
            );
        } catch (IOException e) {
            MainUnit.log(e.toString());
        }
        return result;
    }
}
