/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba;

import com.deutsche.dba.dbutils.DBConnector;
import com.deutsche.dba.dbutils.Instrument;
import com.deutsche.dba.dbutils.InstrumentHandler;
import com.deutsche.dba.dbutils.PropertyLoader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.deutsche.dba.dbutils.User;
import com.deutsche.dba.dbutils.UserHandler;
import java.io.File;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{
   private static final Logger LOGGER = Logger.getLogger(LoggerExample.class.getName());
   public  static  boolean debugFlag = true;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
    }
    
    public  static  void    log( String msg )
    {
        //if( debugFlag )
        {
            LOGGER.info( msg );
            System.out.println( msg );
        }
    }
    
}
