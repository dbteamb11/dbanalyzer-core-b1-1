package com.deutsche.dba.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonHelper {
    static private ObjectMapper mapper = new ObjectMapper();

    public static String toJSON(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }
}
