/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class InstrumentHandler extends RowHandler<Instrument, InstrumentIterator>
{
    public static final String INSTRUMENT_ID_COLUMN_NAME = "instrument_id";
    public static final String INSTRUMENT_NAME_COLUMN_NAME = "instrument_name";
    public static final String INSTRUMENT_TABLE_NAME = "instrument";

    static  private InstrumentHandler instance = null;
    
    private InstrumentHandler(){}
    
    static  public  InstrumentHandler  getLoader()
    {
        if( instance == null )
            instance = new InstrumentHandler();
        return instance;
    }

    @Override
    protected InstrumentIterator getIterator(ResultSet resultSet) {
        return new InstrumentIterator(resultSet);
    }

    public  Instrument  loadFromDB(String dbName, Connection theConnection, int key )
    {
        Instrument result = null;
        try
        {
            String sbQuery = String.format("select * from %s.instrument where instrument_id=?", dbName);
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setInt(1, key);
            result = loadFromDB(stmt);
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    public  ArrayList<Instrument>  loadAllFromDB( Connection theConnection, String dbID )
    {
        ArrayList<Instrument> result = new ArrayList<>();
        try
        {
            String sbQuery = "select * from " + dbID + ".instrument";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            result.addAll(loadFromDBAsList(stmt));
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public  ArrayList<Instrument>  loadFromDB( String dbID, Connection theConnection )
    {
        ArrayList<Instrument> result = new ArrayList<Instrument>();
        try
        {
            String sbQuery = "select * from " + dbID + ".instrument";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            result.addAll(loadFromDBAsList(stmt));
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
}
