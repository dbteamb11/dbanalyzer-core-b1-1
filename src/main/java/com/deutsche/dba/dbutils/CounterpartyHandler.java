package com.deutsche.dba.dbutils;

import com.deutsche.dba.MainUnit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CounterpartyHandler extends RowHandler<Counterparty, CounterpartyIterator>{
    public static final String COUNTERPARTY_ID_COLUMN_NAME = "counterparty_id";
    public static final String COUNTERPARTY_NAME_COLUMN_NAME = "counterparty_name";
    public static final String COUNTERPARTY_STATUS_COLUMN_NAME = "counterparty_status";
    public static final String COUNTERPARTY_DATE_REGISTERED_COLUMN_NAME = "counterparty_date_registered";
    public static final String COUNTERPARTY_TABLE_NAME = "counterparty";

    private static CounterpartyHandler instance = null;

    private CounterpartyHandler(){}

    public static CounterpartyHandler getLoader()
    {
        if( instance == null )
            instance = new CounterpartyHandler();
        return instance;
    }


    @Override
    protected CounterpartyIterator getIterator(ResultSet resultSet) {
        return new CounterpartyIterator(resultSet);
    }

    public Counterparty loadFromDB(Connection connection, String dbName, String counterpartyName) {
        Counterparty result = null;
        try {
            String sbQuery = String.format(
                    "select * from %s.%s where %s=?",
                    dbName,
                    COUNTERPARTY_TABLE_NAME,
                    COUNTERPARTY_NAME_COLUMN_NAME
            );
            PreparedStatement stmt = connection.prepareStatement(sbQuery);
            stmt.setString(1, counterpartyName);
            result = loadFromDB(stmt);
        } catch (SQLException e) {
            MainUnit.log(String.format("Can not get User from database due to error: %s", e.toString()));
        }
        return result;
    }


    public List<Counterparty> getAllCounterparties(Connection theConnection, String dbID)
    {
        List<Counterparty> result = new ArrayList<>();
        try
        {
            String sbQuery = String.format("SELECT * FROM %s.%s",
                    dbID,
                    COUNTERPARTY_TABLE_NAME);
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            ResultSet rs = stmt.executeQuery();
            result.addAll(loadFromDBAsList(stmt));
        }
        catch (SQLException ex)
        {
            MainUnit.log(ex.toString());
        }

        return result;
    }
//
//    public  ArrayList<Instrument>  loadFromDBByCounterpartyName( String dbID, Connection theConnection )
//    {
//        ArrayList<Instrument> result = new ArrayList<Instrument>();
//        Instrument theInstrument = null;
//        try
//        {
//            String sbQuery = "select * from " + dbID + ".instrument";
//            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
//            ResultSet rs = stmt.executeQuery();
//
//            InstrumentIterator iter = new InstrumentIterator(rs);
//
//            while( iter.next() )
//            {
//                theInstrument = iter.buildInstrument();
//                if(MainUnit.debugFlag)
//                    System.out.println( theInstrument.getInstrumentID() + "//" + theInstrument.getInstrumentName() );
//                result.add(theInstrument);
//            }
//        }
//        catch (SQLException ex)
//        {
//            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return result;
//    }
}
