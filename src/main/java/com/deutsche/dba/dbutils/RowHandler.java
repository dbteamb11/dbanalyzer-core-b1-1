package com.deutsche.dba.dbutils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.deutsche.dba.MainUnit;
import com.deutsche.dba.tools.JsonHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class RowHandler<T, I extends RowIterator<T>> {

    public T loadFromDB(PreparedStatement stmt) {
        T result = null;
        if (MainUnit.debugFlag) {
            MainUnit.log(String.format("Executing query: %s", stmt.toString()));
        }
        try
        {
            ResultSet rs = stmt.executeQuery();

            I iter = getIterator(rs);
            
            int count = 0;
            while( iter.next() )
            {
                result = iter.buildObject();
                if(MainUnit.debugFlag && count < 15) {
                    count++;
                    try {
                        MainUnit.log(String.format("Handle entity: %s", JsonHelper.toJSON(result)));
                    } catch (JsonProcessingException e) {
                        //pass
                    }
                }
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public ArrayList<T> loadFromDBAsList(PreparedStatement stmt)
    {
        ArrayList<T> result = new ArrayList<>();
        T entity = null;
        if (MainUnit.debugFlag) {
            MainUnit.log(String.format("Executing query: %s", stmt.toString()));
        }
        try
        {
            ResultSet rs = stmt.executeQuery();

            I iter = getIterator(rs);

            while( iter.next() )
            {
                entity = iter.buildObject();
                result.add(entity);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    abstract protected I getIterator(ResultSet resultSet);
}
