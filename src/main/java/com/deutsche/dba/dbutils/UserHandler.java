/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import com.deutsche.dba.MainUnit;
import com.deutsche.dba.tools.JsonHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author Selvyn
 */
public class UserHandler extends RowHandler<User, UserIterator>
{
    public static final String USER_ID_COLUMN_NAME = "user_id";
    public static final String USER_PWD_COLUMN_NAME = "user_pwd";
    public static final String USERS_TABLE_NAME = "users";

    private static UserHandler instance = null;

    private UserHandler(){}

    static  public  UserHandler  getLoader()
    {
        if( instance == null )
            instance = new UserHandler();
        return instance;
    }

    @Override
    protected UserIterator getIterator(ResultSet resultSet) {
        return new UserIterator(resultSet);
    }

    public  User  loadFromDB(Connection theConnection, String dbName, String userId, String pwd )
    {
        User result = null;
        try {
            MainUnit.log("On Entry -> UserHandler.loadFromDBByCounterpartyName()");
            String sbQuery = String.format(
                    "select * from %s.%s where %s=? and %s=?",
                    dbName,
                    USERS_TABLE_NAME,
                    USER_ID_COLUMN_NAME,
                    USER_PWD_COLUMN_NAME
            );
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            stmt.setString(1, userId);
            stmt.setString(2, pwd);
            result = loadFromDB(stmt);
        } catch (SQLException e) {
            MainUnit.log(String.format("Can not get User from database due to error: %s", e.toString()));
        }
        return result;
    }

    public List<User> getAllUsers(Connection theConnection, String dbName) {
        List<User> result = new ArrayList<>();
        try {
            MainUnit.log("On Entry -> UserHandler.loadFromDBByCounterpartyName()");
            String sbQuery = String.format(
                    "select * from %s.%s",
                    dbName,
                    USERS_TABLE_NAME
            );
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            result.addAll(loadFromDBAsList(stmt));
        } catch (SQLException e) {
            MainUnit.log(String.format("Can not get User from database due to error: %s", e.toString()));
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        DBConnector connector = DBConnector.getInstance();
        Properties pp = new Properties();
        pp.setProperty("dbDriver", "com.mysql.jdbc.Driver");
        pp.setProperty("dbPath", "jdbc:mysql://127.0.0.1:3307/");
        pp.setProperty("dbName", "db_grad");
        pp.setProperty("dbUser", "root");
        pp.setProperty("dbPwd", "ppp");
        System.out.println(connector.connect(pp));
        UserHandler userHandler = new UserHandler();
        List<User> user = userHandler.getAllUsers(connector.getConnection(), "db_grad");
        System.out.println(
                JsonHelper.toJSON(
                        user
                )
        );
    }
}
