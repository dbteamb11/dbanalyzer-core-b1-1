package com.deutsche.dba.dbutils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Deal {
    @Id
    private long id;

    private String time;
    private String type;
    private double amount;
    private int quantity;
    private Counterparty counterparty;

    @ManyToOne
    //TODO: change column name
    @JoinColumn(name = "instrument_id")
    private Instrument instrument;

    public Deal(long id, String time, double amount, int quantity, String type) {
        this.id = id;
        this.time = time;
        this.amount = amount;
        this.quantity = quantity;
        this.type = type;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setType(String type) {
        this.type = type;
    }


    public double getAmount() {
        return amount;
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public int getQuantity() {
        return quantity;
    }

    public long getId() {
        return id;
    }

    public String getTime() {
        return time;
    }

    public void setCounterparty(Counterparty counterparty) {
        this.counterparty = counterparty;
    }

    public Counterparty getCounterparty() {
        return counterparty;
    }

    public String getType() {
        return type;
    }
}
