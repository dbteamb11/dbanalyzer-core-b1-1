/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.deutsche.dba.dbutils.UserHandler.USER_ID_COLUMN_NAME;
import static com.deutsche.dba.dbutils.UserHandler.USER_PWD_COLUMN_NAME;

/**
 *
 * @author Selvyn
 */
public class UserIterator extends RowIterator<User> {

   public UserIterator(ResultSet rs) {
      super(rs);
   }

   public   String  getUserId() throws SQLException
   {
      return getField(USER_ID_COLUMN_NAME);
   }

   public   String  getUserPwd() throws SQLException
   {
      return getField(USER_PWD_COLUMN_NAME);
   }

   @Override
   public User buildObject() throws SQLException
   {
       return new User( getUserId(), getUserPwd());
   }
}
