package com.deutsche.dba.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class RowIterator<T> {
    protected ResultSet rowIterator;

    RowIterator( ResultSet rs )
    {
        rowIterator = rs;
    }

    public boolean  first() throws SQLException
    {
        return rowIterator.first();
    }

    public boolean last() throws SQLException
    {
        return rowIterator.last();
    }
    public boolean next() throws SQLException
    {
        return rowIterator.next();
    }

    public boolean prior() throws SQLException
    {
        return rowIterator.previous();
    }

    public   String  getField(String columnName) throws SQLException
    {
        return rowIterator.getString(columnName);
    }

    abstract public T buildObject() throws SQLException;
}
