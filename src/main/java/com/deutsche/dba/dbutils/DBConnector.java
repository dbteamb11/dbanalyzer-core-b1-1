/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import com.deutsche.dba.MainUnit;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import static com.deutsche.dba.dbutils.CounterpartyHandler.COUNTERPARTY_ID_COLUMN_NAME;
import static com.deutsche.dba.dbutils.CounterpartyHandler.COUNTERPARTY_TABLE_NAME;
import static com.deutsche.dba.dbutils.DealHandler.DEAL_COUNTERPARTY_ID_COLUMN_NAME;
import static com.deutsche.dba.dbutils.DealHandler.DEAL_INSTRUMENT_ID_COLUMN_NAME;
import static com.deutsche.dba.dbutils.DealHandler.DEAL_TABLE_NAME;
import static com.deutsche.dba.dbutils.InstrumentHandler.INSTRUMENT_ID_COLUMN_NAME;
import static com.deutsche.dba.dbutils.InstrumentHandler.INSTRUMENT_TABLE_NAME;

/**
 *
 * @author Selvyn
 */
public class DBConnector
{
    public final static String VIEW_NAME = "all_deals_b11";
    static  private DBConnector instance = null;
    
    private Connection itsConnection;
    private String  dbDriver ="";   //  "com.mysql.jdbc.Driver";
    private String  dbPath = "";    //  "jdbc:mysql://52.209.91.145/";
    private String  dbName = "";    //  "db_grad_cs_1916";
    private String  dbUser = "";    //  "selvyn";
    private String  dbPwd = "";     //  "dbGradProg2016";

    static  public  DBConnector getInstance() throws IOException
    {
        if( instance == null )
             instance = new DBConnector();
        return instance;
    }
    private DBConnector(){}
    
    public  Connection  getConnection()
    {
        return itsConnection;
    }

    public String getDbName() {
        return dbName;
    }

    public void close() {
        try {
            itsConnection.close();
        } catch (SQLException e) {
            MainUnit.log(String.format("Can not close connection due to error: %s", e.toString()));
        }
    }

    private int createView(Connection connection) throws SQLException {
        String viewSql = String.format(
                "create view %s as " +
                        "select * from %s.%s as d " +
                        "join %s.%s as i on d.%s=i.%s " +
                        "join %s.%s as c on d.%s=c.%s;",
                VIEW_NAME,
                dbName,
                DEAL_TABLE_NAME,
                dbName,
                INSTRUMENT_TABLE_NAME,
                DEAL_INSTRUMENT_ID_COLUMN_NAME,
                INSTRUMENT_ID_COLUMN_NAME,
                dbName,
                COUNTERPARTY_TABLE_NAME,
                DEAL_COUNTERPARTY_ID_COLUMN_NAME,
                COUNTERPARTY_ID_COLUMN_NAME
        );
        PreparedStatement preparedStatement = connection.prepareStatement(viewSql);
        return preparedStatement.executeUpdate();
    }

    public boolean connect(Properties properties )
    {
        boolean result = false;
        try
        {
            MainUnit.log("On Entry -> DBConnector.connect()");
            
            dbDriver = properties.getProperty("dbDriver");
            dbPath = properties.getProperty("dbPath");
            dbName = properties.getProperty("dbName");
            dbUser = properties.getProperty("dbUser");
            dbPwd = properties.getProperty("dbPwd");

            MainUnit.log(String.format("Connection properties: %s", properties.toString()));
            Class.forName( dbDriver );

            itsConnection = DriverManager.getConnection(dbPath + dbName, 
                                                    dbUser, 
                                                    dbPwd );
            try {
                createView(itsConnection);
            } catch (SQLException e) {
                MainUnit.log(e.toString());
            }
            /*
            MainUnit.log( itsConnection.getCatalog() );

            DatabaseMetaData metaInfo = itsConnection.getMetaData();

            // The following call returns a result set with following columns
            // TABLE_SCHEM String => schema name
            // TABLE_CATALOG String => catalog name (may be null)
            ResultSet rs = metaInfo.getSchemas();

            CatalogInfoIterator cursor = new CatalogInfoIterator( rs );

            while( cursor.next() )
            {
                System.out.println( cursor.getTable_Schema() + "/" + cursor.getTable_Catalog() );
            }

            rs.close();   
            */
            MainUnit.log( "Successfully connected to " + dbName );
            
            result = true;
        }
        catch( ClassNotFoundException | SQLException e )
        {
           e.printStackTrace();
        }
        
        MainUnit.log("On Exit -> DBConnector.connect()");

        return result;
    }
}
