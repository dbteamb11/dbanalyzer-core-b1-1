package com.deutsche.dba.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.deutsche.dba.dbutils.CounterpartyHandler.*;

public class CounterpartyIterator extends RowIterator<Counterparty> {

    public CounterpartyIterator( ResultSet rs )
    {
        super(rs);
    }

    @Override
    public Counterparty buildObject() throws SQLException
    {
        Counterparty result = new Counterparty(getId(), getName(), getStatus(), getRegisterDate());

        return result;
    }

    private long getId() throws SQLException {
        return rowIterator.getLong(COUNTERPARTY_ID_COLUMN_NAME);
    }

    private String getName() throws SQLException {
        return getField(COUNTERPARTY_NAME_COLUMN_NAME);
    }

    private String getStatus() throws SQLException {
        return getField(COUNTERPARTY_STATUS_COLUMN_NAME);
    }

    private String getRegisterDate() throws SQLException {
        return getField(COUNTERPARTY_DATE_REGISTERED_COLUMN_NAME);
    }
}
