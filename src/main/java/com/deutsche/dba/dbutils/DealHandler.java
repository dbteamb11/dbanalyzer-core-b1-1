package com.deutsche.dba.dbutils;

import com.deutsche.dba.MainUnit;
import com.deutsche.dba.tools.JsonHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static com.deutsche.dba.dbutils.CounterpartyHandler.COUNTERPARTY_NAME_COLUMN_NAME;
import static com.deutsche.dba.dbutils.DBConnector.VIEW_NAME;
import static com.deutsche.dba.dbutils.InstrumentHandler.INSTRUMENT_NAME_COLUMN_NAME;

public class DealHandler extends RowHandler<Deal, DealIterator> {
    public static final String DEAL_ID_COLUMN_NAME = "deal_id";
    public static final String DEAL_TIME_COLUMN_NAME = "deal_time";
    public static final String DEAL_COUNTERPARTY_ID_COLUMN_NAME = "deal_counterparty_id";
    public static final String DEAL_INSTRUMENT_ID_COLUMN_NAME = "deal_instrument_id";
    public static final String DEAL_TYPE_COLUMN_NAME = "deal_type";
    public static final String DEAL_AMOUNT_COLUMN_NAME = "deal_amount";
    public static final String DEAL_QUANTITY_COLUMN_NAME = "deal_quantity";
    public static final String DEAL_TABLE_NAME = "deal";

    static  private DealHandler instance = null;

    private DealHandler(){}

    static  public  DealHandler  getLoader()
    {
        if( instance == null )
            instance = new DealHandler();
        return instance;
    }


    @Override
    protected DealIterator getIterator(ResultSet resultSet) {
        return new DealIterator(resultSet);
    }

    public List<Deal> getFromDBAsListByCounterPartyName(String dbName, Connection theConnection, String counterpartyName) {
        List<Deal> result = new ArrayList<>();
        try {
            String sbQuery = String.format(
                    "select * from %s.%s WHERE %s=? limit 1000",
                    dbName,
                    VIEW_NAME,
                    COUNTERPARTY_NAME_COLUMN_NAME
            );
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            stmt.setString(1, counterpartyName);
            result.addAll(loadFromDBAsList(stmt));
        } catch (SQLException e) {
            MainUnit.log(String.format("Can not get Deals from database due to error: %s", e.toString()));
        }
        return result;
    }

    public List<Deal> getFromDBAsList(String dbName, Connection theConnection) {
        List<Deal> result = new ArrayList<>();
        try {
            String sbQuery = String.format(
                    "select * from %s.%s order by %s DESC limit 1000",
                    dbName,
                    VIEW_NAME,
                    DEAL_TIME_COLUMN_NAME
            );
            System.out.println(sbQuery);
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            result.addAll(loadFromDBAsList(stmt));
        } catch (SQLException e) {
            MainUnit.log(String.format("Can not get Deals from database due to error: %s", e.toString()));
        }
        return result;
    }

    public List<DealPosition> getTimeSeriesByCounterPartyName(String dbName, Connection theConnection, String counterpartyName) {
        List<DealPosition> result = new ArrayList<>();
        try {
            String setVariable = "SET @csum := 0;";
            PreparedStatement preparedStatement = theConnection.prepareStatement(setVariable);
            preparedStatement.executeUpdate();

            String subQuery = String.format(
                    "SELECT\n" +
                            "    @csum := @csum + (IF(%s = 'B', %s*(-1), %s))*%s as current_position, \n" +
                            "    %s,\n" +
                            "    %s\n" +
                            "    FROM %s.%s\n" +
                            "    WHERE  %s = (SELECT %s FROM %s.%s WHERE %s=?)\n" +
                            "    order by %s limit 1000;",
                    DEAL_TYPE_COLUMN_NAME,
                    DEAL_AMOUNT_COLUMN_NAME,
                    DEAL_ID_COLUMN_NAME,
                    DEAL_QUANTITY_COLUMN_NAME,
                    DEAL_TIME_COLUMN_NAME,
                    DEAL_ID_COLUMN_NAME,
                    dbName,
                    DEAL_TABLE_NAME,
                    DEAL_COUNTERPARTY_ID_COLUMN_NAME,
                    CounterpartyHandler.COUNTERPARTY_ID_COLUMN_NAME,
                    dbName,
                    CounterpartyHandler.COUNTERPARTY_TABLE_NAME,
                    COUNTERPARTY_NAME_COLUMN_NAME,
                    DEAL_TIME_COLUMN_NAME
            );
            preparedStatement = theConnection.prepareStatement(subQuery);
            preparedStatement.setString(1, counterpartyName);
            ResultSet resultSet = preparedStatement.executeQuery();
            DealPositionIterator iter = new DealPositionIterator(resultSet);

            while( iter.next() )
            {
                DealPosition entity = iter.buildObject();
                result.add(entity);
            }
        } catch (SQLException e) {
            MainUnit.log(e.toString());
        }
        return result;
    }

    public List<DealPosition> getTimeSeriesByInstrumentName(String dbName, Connection theConnection, String instrumentName) {
        List<DealPosition> result = new ArrayList<>();
        try {
            String setVariable = "SET @csum := 0;";
            PreparedStatement preparedStatement = theConnection.prepareStatement(setVariable);
            preparedStatement.executeUpdate();

            String subQuery = String.format(
                    "SELECT\n" +
                            "    @csum := @csum + (IF(%s = 'B', %s*(-1), %s))*%s as current_position, \n" +
                            "    %s,\n" +
                            "    %s\n" +
                            "    FROM %s.%s\n" +
                            "    WHERE  %s=?\n" +
                            "    order by %s limit 1000;",
                    DEAL_TYPE_COLUMN_NAME,
                    DEAL_AMOUNT_COLUMN_NAME,
                    DEAL_ID_COLUMN_NAME,
                    DEAL_QUANTITY_COLUMN_NAME,
                    DEAL_TIME_COLUMN_NAME,
                    DEAL_ID_COLUMN_NAME,
                    dbName,
                    VIEW_NAME,
                    INSTRUMENT_NAME_COLUMN_NAME,
                    DEAL_TIME_COLUMN_NAME
            );
            preparedStatement = theConnection.prepareStatement(subQuery);
            preparedStatement.setString(1, instrumentName);
            ResultSet resultSet = preparedStatement.executeQuery();
            DealPositionIterator iter = new DealPositionIterator(resultSet);

            while( iter.next() )
            {
                DealPosition entity = iter.buildObject();
                result.add(entity);
            }
        } catch (SQLException e) {
            MainUnit.log(e.toString());
        }
        return result;
    }

    public List<Deal> getFromDBAsListByInstrumentName(String dbName, Connection connection, String instrumentName) {
        List<Deal> result = new ArrayList<>();
        try {
            String sbQuery = String.format(
                    "select * from %s.%s WHERE %s=? limit 1000",
                    dbName,
                    VIEW_NAME,
                    INSTRUMENT_NAME_COLUMN_NAME
            );
            PreparedStatement stmt = connection.prepareStatement(sbQuery);
            stmt.setString(1, instrumentName);
            result.addAll(loadFromDBAsList(stmt));
        } catch (SQLException e) {
            MainUnit.log(String.format("Can not get Deals from database due to error: %s", e.toString()));
        }
        return result;

    }


    public class DealPosition {
        private long id;
        private String time;
        private double currentPosition;

        DealPosition(long id, String time, double currentPosition) {
            this.id = id;
            this.time = time;
            this.currentPosition = currentPosition;
        }

        public void setId(long id) {
            this.id = id;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public void setCurrentPosition(double currentPosition) {
            this.currentPosition = currentPosition;
        }

        public long getId() {
            return id;
        }

        public String getTime() {
            return time;
        }

        public double getCurrentPosition() {
            return currentPosition;
        }
    }

    private class DealPositionIterator extends RowIterator<DealPosition> {
        DealPositionIterator(ResultSet rs) {
            super(rs);
        }

        @Override
        public DealPosition buildObject() throws SQLException {
            return new DealPosition(
                    rowIterator.getLong(DEAL_ID_COLUMN_NAME),
                    getField(DEAL_TIME_COLUMN_NAME),
                    rowIterator.getDouble("current_position")
            );
        }
    }
}
