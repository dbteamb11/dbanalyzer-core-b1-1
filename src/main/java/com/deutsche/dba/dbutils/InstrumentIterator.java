/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.deutsche.dba.dbutils.InstrumentHandler.INSTRUMENT_ID_COLUMN_NAME;
import static com.deutsche.dba.dbutils.InstrumentHandler.INSTRUMENT_NAME_COLUMN_NAME;

/**
 *
 * @author Selvyn
 */
public class InstrumentIterator extends RowIterator<Instrument>
{

   InstrumentIterator( ResultSet rs )
   {
       super(rs);
   }

   public String getInstrumentName() throws SQLException
   {
      return rowIterator.getString(INSTRUMENT_NAME_COLUMN_NAME);
   }

   public int getInstrumentID() throws SQLException
   {
      return rowIterator.getInt(INSTRUMENT_ID_COLUMN_NAME);
   }

   @Override
   public Instrument buildObject() throws SQLException
   {
       return new Instrument( getInstrumentID(), getInstrumentName() );
   }
}
