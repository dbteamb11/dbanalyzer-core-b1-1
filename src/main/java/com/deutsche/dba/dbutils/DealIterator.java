package com.deutsche.dba.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.deutsche.dba.dbutils.DealHandler.*;

public class DealIterator extends RowIterator<Deal> {

    public DealIterator(ResultSet rs) {
        super(rs);
    }

    @Override
    public Deal buildObject() throws SQLException {
        Deal deal = new Deal(getId(), getTime(), getAmount(), getQuantity(), getType());
        try {
            CounterpartyIterator counterpartyIterator = new CounterpartyIterator(rowIterator);
            deal.setCounterparty(counterpartyIterator.buildObject());
        } catch (Exception e) {

        }
        try {
            InstrumentIterator instrumentIterator = new InstrumentIterator(rowIterator);
            deal.setInstrument(instrumentIterator.buildObject());
        } catch (Exception e) {

        }
        return deal;
    }

    private String getType() throws SQLException {
        return getField(DEAL_TYPE_COLUMN_NAME);
    }

    private long getId() throws SQLException {
        return rowIterator.getInt(DEAL_ID_COLUMN_NAME);
    }

    private String getTime() throws SQLException {
        return getField(DEAL_TIME_COLUMN_NAME);
    }

    private double getAmount() throws SQLException {
        return rowIterator.getDouble(DEAL_AMOUNT_COLUMN_NAME);
    }

    private int getQuantity() throws SQLException {
        return rowIterator.getInt(DEAL_QUANTITY_COLUMN_NAME);
    }
}
