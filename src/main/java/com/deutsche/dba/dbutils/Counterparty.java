package com.deutsche.dba.dbutils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Counterparty implements Serializable {
    @Id
    private long id;

    private String name;
    private String status;
    private String dateRegister;

//    @OneToMany(mappedBy = "counterparty")
//    private List<Deal> deals;

    public Counterparty(long id, String name, String status, String dateRegister) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.dateRegister = dateRegister;
//        this.deals = new ArrayList<>();
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

//    public void setDeals(List<Deal> deals) {
//        this.deals = deals;
//    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

//    public List<Deal> getDeals() {
//        return deals;
//    }

    public String getDateRegister() {
        return dateRegister;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

//    public void addDeal(Deal deal) {
//        deals.add(deal);
//    }
}
