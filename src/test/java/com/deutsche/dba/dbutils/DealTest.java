/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Graduate
 */
public class DealTest {
    

    private Deal deal;

    @Before
    public void setUp() throws Exception {
        deal = new Deal();
    }

    
    
     @Test
    public void checkDealId() {

        deal.setId(2001);
        Assert.assertEquals(2001, deal.getId());


    }
    
    
     @Test
    public void checkDeal_Time() {

        deal.setTime("2018-07-30 05:58:21");
        Assert.assertEquals("2018-07-30 05:58:21", deal.getTime());


    }
    
    
    @Test
    public void checkDeal_Type() {

        deal.setType("S");
        Assert.assertEquals("S", deal.getType());
        


    }
    
    
    
    @Test
    public void checkDeal_Amount() {

        deal.setAmount(3405.29);

         assertEquals(3405.29, deal.getAmount(), 0.01);

    }

    @Test

    public void checkDeal_Quantity() {

        deal.setQuantity(6);
        Assert.assertEquals(6, deal.getQuantity());
    }

  

}