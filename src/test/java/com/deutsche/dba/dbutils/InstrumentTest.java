/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Graduate
 */
public class InstrumentTest {
    
    
    private Instrument instrument;

    @Before
    public void setUp() throws Exception {
        instrument = new Instrument();
    }

    @Test
    public void getInstrumentId() {

        instrument.setInstrumentID(1);

        Assert.assertEquals(1, instrument.getInstrumentID());

    }

    @Test
    public void getInstrumentName() {

        instrument.setInstrumentName("Astronomica");

        Assert.assertEquals("Astronomica", instrument.getInstrumentName());

    }
    
}
