/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import com.deutsche.dba.tools.JsonHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Graduate
 */
public class UserHandlerTest {
    
    private static DBConnector connector;
    
    
    @BeforeClass
   public static void setUp() throws Exception {
        connector = DBConnector.getInstance();
        Properties pp = new Properties();
        pp.setProperty("dbDriver", "com.mysql.jdbc.Driver");
        pp.setProperty("dbPath", "jdbc:mysql://10.11.32.21:3306/");
        pp.setProperty("dbName", "db_grad_cs_1917");
        pp.setProperty("dbUser", "dbgrad");
        pp.setProperty("dbPwd", "dbgrad");
        connector.connect(pp);
        }
        //Test Case with correct login details
        @Test
        public void loadFromDBTest() throws Exception {
        UserHandler handler = UserHandler.getLoader();
        
        String userid ="selvyn";
        String pwd = "gradprog2016";
        String dbname ="db_grad_cs_1917";
        
        User user = handler.loadFromDB(connector.getConnection(),dbname, userid, pwd);
         
        assertEquals(userid, user.getUserID());
        assertEquals(pwd, user.getUserPwd());
    }
     
        //Test case with username given as CAPS and convert To Lower Case for a sucessful Test run.
     @Test
        public void loadFromDBToLowerTest() throws Exception {
        UserHandler handler = UserHandler.getLoader();
        
        String userid ="DEBS";
        String pwd = "gradprog2016@02";
        String dbname ="db_grad_cs_1917";
        
        User user = handler.loadFromDB(connector.getConnection(),dbname, userid, pwd);
        
        assertEquals(userid.toLowerCase(), user.getUserID().toLowerCase());
        assertEquals(pwd, user.getUserPwd());
    }
   
    
    
}




