/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Graduate
 */
public class UserTest {
    
    private User usertest;

    @Before
    public void setUp() throws Exception {
        usertest = new User();
    }
   
     @Test
    public void checkUser_ID() {

        usertest.setUserID("alison");
        Assert.assertEquals("alison", usertest.getUserID());
}
          
     @Test
    public void checkUser_Password() {

        usertest.setUserPwd("gradprog2016@07");
        Assert.assertEquals("gradprog2016@07", usertest.getUserPwd());
        
}
   

}
