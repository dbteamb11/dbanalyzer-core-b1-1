/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Graduate
 */
public class CounterpartyTest {
    
   private Counterparty counterparty;

    @Before
    public void setUp() throws Exception {
        this.counterparty = new Counterparty();
    }
    
    @Test
    public void getID() {
        
    counterparty.setId(5);
    Assert.assertEquals(5, counterparty.getId());
        
    }

@Test
     public void getName() {
        counterparty.setName("Lina");
        Assert.assertEquals("Lina", counterparty.getName());
    }

     
     
     @Test
    public void getStatus() {
        counterparty.setStatus("A");
        Assert.assertEquals("A", counterparty.getStatus());
    }


    @Test
    public void getDateRegister() {
        counterparty.setDateRegister("2014-01-01 00:00:00");
        Assert.assertEquals("2014-01-01 00:00:00",counterparty.getDateRegister());
     
     
  }

}

