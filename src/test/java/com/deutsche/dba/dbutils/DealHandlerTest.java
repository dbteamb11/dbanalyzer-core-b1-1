/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.dbutils;

import com.deutsche.dba.tools.JsonHelper;
import static com.deutsche.dba.tools.JsonHelper.toJSON;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Graduate
 */
public class DealHandlerTest {
    
    private static DBConnector connector;
    
public static void setUp() throws Exception {
        connector = DBConnector.getInstance();
        Properties pp = new Properties();
        pp.setProperty("dbDriver", "com.mysql.jdbc.Driver");
        pp.setProperty("dbPath", "jdbc:mysql://10.11.32.21:3306/");
        pp.setProperty("dbName", "db_grad_cs_1917");
        pp.setProperty("dbUser", "dbgrad");
        pp.setProperty("dbPwd", "dbgrad");
        connector.connect(pp);
        }
   
   
    @Test
    public void getDealsByInstrumentNameTest() throws Exception {
        DealHandler handler = DealHandler.getLoader();
       
        System.out.println(JsonHelper.toJSON(
                handler.getTimeSeriesByInstrumentName("db_grad_cs_1917", connector.getConnection(), "Celestial")
                )
          );
    }
        
     @Test
    public void getFromDBAsListByCounterPartyNameTest() throws Exception {
        DealHandler handler = DealHandler.getLoader();
       
        System.out.println(JsonHelper.toJSON(
                handler.getTimeSeriesByCounterPartyName("db_grad_cs_1917", connector.getConnection(), "Lina")
                )
          );
       
    }
    
    
 
       @Test
      public void getFromDBAsListTest() throws Exception {
        DealHandler handler = DealHandler.getLoader();
       
        System.out.println(JsonHelper.toJSON(
                handler.getFromDBAsList("db_grad_cs_1917", connector.getConnection())
                )
          );
       
    }
   
}
