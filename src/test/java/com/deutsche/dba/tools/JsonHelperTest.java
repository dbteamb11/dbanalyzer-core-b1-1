/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.tools;

import com.deutsche.dba.dbutils.Counterparty;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Graduate
 */
public class JsonHelperTest {
    
    public JsonHelperTest() {
    }

    /**
     * Test of toJSON method, of class JsonHelper.
     */
    @Test
    public void testToJSON() throws Exception {
        Counterparty counterparty = new Counterparty(0, "Yash","Open","29:06:2018");
        String Expected = "{\"id\":0,\"name\":\"Yash\",\"status\":\"Open\",\"dateRegister\":\"29:06:2018\"}"; 
        String actualValue = JsonHelper.toJSON(counterparty);
        assertEquals(Expected, actualValue);
    }
}
